---
title: "Press"
layout: "about"
draft: false

# what_we_do
what_we_do:
  enable: true
  title: "<br>Press Coverage"
  block:
  - title: "MakeUseOf"
    content: "\"While still young and under development, Ubuntu Web is giving users a choice with a full suite of privacy-respecting, open-source alternatives that stand up well against Google’s notoriously information-hungry web OS and apps.\" **- JT McGinty**

    <br><br>[**Read more >>**](https://www.makeuseof.com/ubuntu-web-chrome-os-alternative/)"

  - title: "9to5Linux"
    content: "\"While it has an emphasis on Web Apps, Ubuntu Web Remix looks and feel like a real GNU/Linux distribution that you can easily put on your desktop or laptop computer...<br><br>But the biggest new feature is /e/ on WayDroid, a port of the popular Anbox alternative WayDroid to /e/ 10, allowing users to use Android or /e/ apps with native performance from the /e/ store in Ubuntu Web.\" **- Marius Nestor**

    <br><br>[**Read more >>**](https://9to5linux.com/ubuntu-web-remix-20-04-3-released-with-e-on-waydroid-and-linux-kernel-5-11/)"

  - title: "TechRepublic"
    content: "\"If you're looking for an operating system that functions similarly to Chrome OS but want to cut ties with Google, Ubuntu Web might well be the ideal platform. This Linux distribution is easy to use, and works seamlessly with the /e/ ecosystem. All the while, Ubuntu Web can function as a full-blown Linux operating system, so it's like getting the best of both worlds.\" **- Jack Wallen**

    <br><br>[**Read more >>**](https://www.techrepublic.com/article/linux-finally-has-an-impressive-cloud-like-os-in-ubuntu-web/)"

# our team
our_team:
  enable: true
  subtitle: "<br><br>Our developers"
  title: "Team"
  team:
  - name: "Rudra (`@rs2009`)"
    image: "images/about/team/rs2009.gif"
    designation: "Creator, Project Lead, website maintainer, dev & Ubuntu Member"

---