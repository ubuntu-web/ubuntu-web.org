---
# banner
banner:
  title: "A powerful, free/libre web OS."
  button: "Download"
  button_link: "download/"
  image: "images/laptop.png"


# brands
brands_carousel:
  enable: false
  brand_images:
  - " "


# how_it_works
how_it_works:   
  enable: true
  block:
  - subtitle: ""
    title: "Web Store"
    description: "We have made a new web store with a large app catalog, containing apps from the /e/ store and those submitted by the Ubuntu Web community."
    image: "images/about/screenshots/web-store.png"

  - subtitle: "Firefox and Brave variants"
    title: "Choose your preferred browser"
    description: "You can choose between Firefox and Brave, both of which have in-built adblockers (and Brave even offers support for an incognito mode with Tor out-of-the-box!)"
    image: "images/blog/ubuntu-web-20.04.4-with-brave.png"

  - subtitle: "Install any phone app on your Ubuntu Web PC"
    title: "Android App Support"
    description: "The '/e/ on WayDroid' app allows you to install any app from the pre-installed /e/ Android app store. You can also install other stores like F-Droid or the Aurora Store to install apps from them."
    image: "images/about/screenshots/android-apps.jpg"


# intro_video
intro_video:   
  enable: true
  subtitle: "The Linux Experiment"
  title: "A review of Ubuntu Web 20.04"
  description: "A video about the first release of Ubuntu Web (released in Dec 2020)."
  video_url: "https://www.youtube.com/embed/ugAB_ug9MZg"
  video_thumbnail: "images/linuxexperiment.jpg"

# testimonials
testimonials:   
  enable: true
  title: "<br><br>Testimonials"
  image_left: "none"
  image_right: "none"
  
  testimonials_quotes:
  - quote: "While still young and under development, Ubuntu Web is giving users a choice with a full suite of privacy-respecting, open-source alternatives that stand up well against Google’s notoriously information-hungry web OS and apps."
    name: "JT McGinty"
    designation: "MakeUseOf"
    image: "none"

  - quote: "While it has an emphasis on Web Apps, Ubuntu Web Remix looks and feel like a real GNU/Linux distribution that you can easily put on your desktop or laptop computer... But the biggest new feature is /e/ on WayDroid, a port of the popular Anbox alternative WayDroid to /e/ 10, allowing users to use Android or /e/ apps with native performance from the /e/ store in Ubuntu Web."
    name: "Marius Nestor"
    designation: "9to5Linux"
    image: "none"

  - quote: "If you're looking for an operating system that functions similarly to Chrome OS but want to cut ties with Google, Ubuntu Web might well be the ideal platform. This Linux distribution is easy to use, and works seamlessly with the /e/ ecosystem. All the while, Ubuntu Web can function as a full-blown Linux operating system, so it's like getting the best of both worlds."
    name: "Jack Wallen"
    designation: "Tech Republic"
    image: "none"

---
