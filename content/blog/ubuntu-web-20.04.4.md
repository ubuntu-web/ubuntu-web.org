---
title: "Ubuntu Web 20.04.4 and the new Brave variant"
date: 2022-03-04T15:32:45+05:30
image: "images/blog/ubuntu-web-20.04.4-with-brave.png"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
---

Ubuntu Web 20.04.4 has now been released! You can get it from https://ubuntu-web.org/download. For extremely old machines, the BIOS ISO is also available.

This release includes a new Brave variant, which uses the Brave Browser with better PWA support and more privacy-related features (including a built-in Tor mode) but the same /e/ store.

It also includes ‘/e/ on WayDroid’. WayDroid is a new popular Anbox alternative for running Android apps and we have ported /e/ 10 to it, along with making a tool to manage it. So, in addition to the PWAs from the /e/ web store, you can now use Android or /e/ apps with native performance from the /e/ Android app store in Ubuntu Web. After installing the ISO, you can just launch the ‘/e/ on WayDroid’ app from the launcher and follow the steps. You’ll need a real machine (not a VM) for WayDroid to work.

Here’s how to install and use the phone (Android) apps on Ubuntu Web: https://twitter.com/ubunweb/status/1459911680264073221

Here’s how to use the web apps on Ubuntu Web: https://twitter.com/ubunweb/status/1459915399647035396

Although WayDroid is quite stable now, occassional issues can be fixed by restarting it from the ‘/e/ on Waydroid’ tool available in the Ubuntu Web ISO. Note that since WayDroid doesn’t support Nvidia for obvious reasons, if you have an Nvidia GPU, you’ll either need to use the iGPU if your CPU has one or use software-rendering (which might make things slow). Also, WayDroid doesn’t work on live sessions at the moment.