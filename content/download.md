---
title: "Download"
layout: "single"
draft: false
---

<h2 style="text-align: center;"></h2>

<p style="text-align: center;">
    <a href="http://linux.darkpenguin.net/distros/ubuntu-unity/ubuntu-web/20.04.4/brave" class="btn btn-primary" style="color: white; text-decoration: none; margin: 15px;">Download Brave Edition</a>
    <a href="http://linux.darkpenguin.net/distros/ubuntu-unity/ubuntu-web/20.04.4/firefox" class="btn btn-primary" style="color: white; text-decoration: none;">Download Firefox Edition</a>
    <br>
    <a href="https://fosstorrents.com/distributions/ubuntu-web/" class="btn btn-primary" style="color: white; text-decoration: none; margin: 15px;">Download torrent</a>
</p>x